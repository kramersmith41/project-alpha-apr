from django.shortcuts import render
from tasks.forms import TaskForm
from django.shortcuts import redirect
from django.contrib.auth.decorators import login_required
from tasks.models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    my_tasks_list = Task.objects.filter(assignee=request.user)
    context = {
        "my_tasks_list": my_tasks_list,
    }
    return render(request, "tasks/list.html", context)
