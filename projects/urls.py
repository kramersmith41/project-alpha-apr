from django.urls import path
from projects.views import (
    create_project,
    list_projects,
    show_project,
    game_view,
)


urlpatterns = [
    path("game/", game_view, name="game_view"),
    path("", list_projects, name="list_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
]
